## To Do List:

###### Functional Testing
* 3D View
    * <s>Design test</s>
    * <s>Implement tests</s>
* Geometry View
    * <s>Design test</s>
    * Implement tests WIP
        * currently working on the calendar input
* GraphView
    * <s>Design test</s>
    * Implement tests
###### Integration Testing
* FE - BE
* BE - DB
###### Load/Stress Testing
* generate script
###### API Testing
* <s>camera</s>
* <s>imageProcessing</s>
* <s>robot</s>
* <s>ultraSonic</s>
* <s>cell</s>


## Instructions

To execute on localhost, just execute 2 instructions:
##### `npm install`
##### `npm run test`


