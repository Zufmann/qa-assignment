## API testing

#### Camera

###### Happy path
* spotweld/v1/{CellID}/camera/command/initialize
    * check:
    *  spotweld/v1/{CellID}/camera/event/initialized
    *  spotweld/v1/{CellID}/camera/state/imagesPath
* spotweld/v1/{CellID}/camera/command/captureImage
    * check:
    *  spotweld/v1/{CellID}/camera/state/currentImage
* spotweld/v1/{CellID}/camera/command/stop
    * check:
    *  spotweld/v1/{CellID}/camera/event/stopped
###### Negative path
* spotweld/v1/{CellID}/camera/command/captureImage
    * check:
    *  spotweld/v1/{CellID}/camera/error
* spotweld/v1/{CellID}/camera/command/stop
    * check:
    *  spotweld/v1/{CellID}/camera/error


#### ImageProcessing

###### Happy path
* spotweld/v1/{CellID}/imageProcessing/command/detectSpurAndPosition
    * check:
    *  spotweld/v1/{CellID}/imageProcessing/state/spurAndPositionDetected
* spotweld/v1/{CellID}/imageProcessing/command/calculateBlur
    * check:
    *  spotweld/v1/{CellID}/imageProcessing/state/blurCalculated
###### Negative path


#### robot

###### Happy path
* spotweld/v1/{CellID}/robot/command/service
    * check:
    *  spotweld/v1/{CellID}/robot/state/timeToService
    *  spotweld/v1/{CellID}/robot/state/probeCondition
###### Negative path


#### ultraSonic

###### Happy path
* spotweld/v1/{CellID}/ultraSonic/command/startTestPlan
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/event/testPlanStarted
    *  spotweld/v1/{CellID}/ultraSonic/state/testPlanTree
* spotweld/v1/{CellID}/ultraSonic/command/synchronize
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/event/synchronized
* spotweld/v1/{CellID}/ultraSonic/command/startMeasurement
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/event/measurementComplete
* spotweld/v1/{CellID}/ultraSonic/command/startScan
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/event/alignmentAngle
    *  spotweld/v1/{CellID}/ultraSonic/state/activeTestPlan
* spotweld/v1/{CellID}/ultraSonic/command/cancelScan
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/event/scanCanceled
    *  spotweld/v1/{CellID}/ultraSonic/state/mode
###### Negative path
* spotweld/v1/{CellID}/ultraSonic/command/synchronize
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/error
* spotweld/v1/{CellID}/ultraSonic/command/startMeasurement
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/error
* spotweld/v1/{CellID}/ultraSonic/command/cancelScan
    * check:
    *  spotweld/v1/{CellID}/ultraSonic/error


#### cell

###### Happy path
* spotweld/v1/{CellID}/cell/command/acknowledgeAlarm
    * check:
    *  spotweld/v1/{CellID}/cell/event/alarmAcknowledged
    *  spotweld/v1/{CellID}/cell/state/measurementState
    *  spotweld/v1/{CellID}/cell/state/alarms
    *  spotweld/v1/{CellID}/cell/state/membraneStatus
    *  spotweld/v1/{CellID}/cell/state/equipmentStatus
###### Negative path