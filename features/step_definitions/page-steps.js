const { expect } = require('chai');
const World = require('../support/world');
const {Given} = require('cucumber');


Given(/^I have navigated to the (.+) page$/, {timeout: 15000}, function(pageName) {
  this.page = this.pageFactory.create(pageName);
  return this.page.loadAndWaitUntilVisible();
});
