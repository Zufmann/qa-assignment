const { expect } = require('chai');
const World = require('../support/world');
const {When, Then} = require('cucumber');


When(/^page is fully loaded$/,{timeout: 15 * 1000}, function() {
  return this.page.waitForPageToLoad();
});

When(/^on the (.+)$/, function(tabName) {
  return this.page.goToTab(tabName);
});

Then(/^there should be (\d+) features shown$/, function(number) {
  return this.page.numberOfResults().then((numberOfResults) => expect(numberOfResults).to.equal(number));
});

Then(/^the feature (.+) is (red|green|yellow)$/, function(featureName, color) {
  return this.page.selectFeature(featureName).then((feature) => {
    feature.getAttribute("src").then(image =>  expect(image).to.include(toTitleCase(color)));
  });
});

function toTitleCase(str) {
  return str.replace(/(^|\s)\S/g, function(t) { return t.toUpperCase(); });
}