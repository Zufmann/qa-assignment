class Qrt3dElements{

  get pageElements() {
    return {
      title: 'quality-report-tool',
      featuresList: '//div[contains(@class,"feature-list")]',
      featuresListed: '//div[contains(@class,"feature-list")]//button',
      faderLoader: '//div[contains(@class,"loading-screen")]',
      featuresListedByName: (name) => `//div[contains(@class,"feature-list")]//button[text()[contains(.,"${name}")]]//div[1]//img`,
      tabLink: (tabName) => `//li[text()[contains(.,"${tabName}")]]/div`,
      geometryInputFrom: '//div[contains(@class,"list-view-filters__control-date")]//div[contains(@class,"list-view-filters__control")][1]//input'
    }
  }
}

module.exports = new Qrt3dElements();