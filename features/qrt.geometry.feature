Feature: QRT Geometry View
  In order to know to measure process
  As a user
  I want to be able to see all the feature of a manufacturing part and its measures

  Scenario: Search all different options to features
    Given I have navigated to the ABB QRT page
    When page is fully loaded
    And on the Geometry view
    And a certain date is selected
    Then there should be 6 graphs shown
    And the are 20 pieces analyzed