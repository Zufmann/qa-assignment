const { driver, browser } = require('./driver');
const Screenshot = require('../selenium/Screenshot');
const PageFactory = require('../pages/Factory');
const {setWorldConstructor} = require('cucumber');

const World = function World() {
  this.driver = driver;
  this.browser = browser;

  this.screenshot = new Screenshot(this.browser);
  this.screenshot.ensureDirectoryExists();

  this.pageFactory = new PageFactory(this);
};

setWorldConstructor(World);

module.exports = World;