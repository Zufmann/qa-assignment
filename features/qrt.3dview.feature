Feature: QRT 3d View
  In order to know to measure process
  As a user
  I want to be able to see all the feature of a manufacturing part and its measures

  Scenario: Search all different options to features
    Given I have navigated to the ABB QRT page
    When page is fully loaded
    And on the 3D View
    Then there should be 12 features shown
    Then the feature PUNZON 15 is red
    Then the feature 701_W__MA_0001 is yellow
    Then the feature 706_AB_MA_0001 is green