const GoogleSearch = require('./GoogleSearch');
const QRT = require('./qrt');
const NotFoundError = require('./NotFoundError');


module.exports = class Factory {

  constructor(World) {
    this.World = World;
    this.pages = {
      "Google Search": GoogleSearch,
      "ABB QRT": QRT
    };
  }

  create(name) {
    const pageClass = this.pages[name];
    if (pageClass == null) throw new NotFoundError(name);
    return new pageClass(this.World);
  }

};