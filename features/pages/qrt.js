const Base = require('./Base');
const qrtElements = require('../elements/qrt-3d.elements');
const {By} = require('selenium-webdriver');


module.exports = class QRT extends Base {

  constructor(World) {
    super(World);
  }

  load() {
    this.browser.get("http://qrt-assignment.northeurope.cloudapp.azure.com");
  }

  waitUntilVisible() {
    return this.waitUntilElementIsVisible(By.xpath(qrtElements.pageElements.featuresList));
  }

  numberOfResults() {
    return this.browser.findElements(By.xpath(qrtElements.pageElements.featuresListed)).then((elements) => elements.length);
  }

  selectFeature(featureName) {
    return this.browser.findElement(By.xpath(qrtElements.pageElements.featuresListedByName(featureName)));
  }

  goToTab(tabName){
    return this.browser.findElement(By.xpath(qrtElements.pageElements.tabLink(tabName))).then(link => link.click())
  }

  waitForPageToLoad(){
    return this.waitUntilElementIsNotVisible(By.xpath((qrtElements.pageElements.faderLoader)))
  }

  waitUntilElementIsNotVisible(locator) {
    return this.browser.wait(() => {
      console.log('inside wait');
      return this.browser.findElements(locator).then(function(found) {
        console.log('inside wait findE');
        return found.length === 0;
      });
    }, 3000, 'The element should disappear');
  }

};